/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


 #ifndef _fbx2json_utils_hpp_
 #define _fbx2json_utils_hpp_
  
 #include <vector>
 
 #define _USE_MATH_DEFINES
 #ifdef _WIN32
 #ifdef max
 #undef max
 #endif
 #ifdef min
 #undef min
 #endif
 #else
 #include <math.h>
 #endif
 
 namespace fbx2json {
     
 extern const float kPi;
 extern const float kPiOver2;
 extern const float kPiOver4;
 extern const float k2Pi;
 extern const float kPiOver180;
 extern const float k180OverPi;
 
 namespace trigonometry {
 
     extern float degreesToRadians(float d);
     extern float radiansToDegrees(float r);
     extern float sin(float f);
     extern float cos(float f);
     extern float tan(float f);
     extern float asin(float f);
     extern float acos(float f);
     extern float cotan(float i);
     extern float atan(float i);
     extern float atan2(float i, float j);
     
     extern const float deg;
     extern const float rad;
 }
 
 namespace number {
     
     extern  short minValue(short val);
     extern  short maxValue(short val);
     extern  int minValue(int val);
     extern  int maxValue(int val);
     extern  long minValue(long val);
     extern  long maxValue(long val);
     extern  long long minValue(long long val);
     extern  long long maxValue(long long val);
     extern  unsigned short minValue(unsigned short val);
     extern  unsigned short maxValue(unsigned short val);
     extern  unsigned int minValue(unsigned int val);
     extern  unsigned int maxValue(unsigned int val);
     extern  unsigned long minValue(unsigned long val);
     extern  unsigned long maxValue(unsigned long val);
     extern  unsigned long long minValue(unsigned long long val);
     extern  unsigned long long maxValue(unsigned long long val);
     extern  float minValue(float val);
     extern  float maxValue(float val);
     extern  double minValue(double val);
     extern  double maxValue(double val);
     
     template <class T> T clamp(T val, T min, T max) { return (val<min ? min:(val>max ? max:val)); }
 }
 
     
 extern  float sqrt(float v);
 extern  short max(short a,short b);
 extern  unsigned short max(unsigned short a,unsigned short b);
 extern  int max(int a,int b);
 extern  unsigned int max(unsigned int a,unsigned int b);
 extern  long max(long a,long b);
 extern  unsigned long max(unsigned long a,unsigned long b);
 extern  long long max(long long a,long long b);
 extern  unsigned long long max(unsigned long long a,unsigned long long b);
 extern  float max(float a,float b);
 extern  double max(double a,double b);
 extern  short min(short a,short b);
 extern  unsigned short min(unsigned short a,unsigned short b);
 extern  int min(int a,int b);
 extern  unsigned int min(unsigned int a,unsigned int b);
 extern  long min(long a,long b);
 extern  unsigned long min(unsigned long a,unsigned long b);
 extern  long long min(long long a,long long b);
 extern  unsigned long long min(unsigned long long a,unsigned long long b);
 extern  float min(float a,float b);
 extern  double min(double a,double b);
 extern  short abs(short a);
 extern  int abs(int a);
 extern  long abs(long a);
 extern  long long abs(long long a);
 extern  float abs(float a);
 extern  double abs(double a);
 extern  float random();
 extern  float lerp(float from, float to, float t);
 extern  bool isPowerOfTwo(int x);
 extern  int nextPowerOfTwo(int x);
 extern	float round(float n);
 extern	double round(double n);
 
 namespace distance {
 
 extern const float Km;
 extern const float Hm;
 extern const float Dam;
 extern const float meter;
 extern const float dm;
 extern const float cm;
 extern const float mm;
 extern const float inch;
 extern const float feet;
 extern const float yard;
 extern const float mile;
 
 }
 
 class Scalar {
 public:
     Scalar() :_value(0.0f) {}
     template <class T>
     Scalar(T v, float factor = distance::meter) :_value(static_cast<float>(v) / factor) {}
     
     inline float value() const { return _value; }
     
     template <class T>
     inline void setValue(T v, float factor = distance::meter) { _value = static_cast<float>(v) / factor; }
 
     inline Scalar & operator =(const Scalar & s) { _value = s._value; return *this; }
     inline bool operator ==(const Scalar & s) { return _value == s._value; }
     
 protected:
     float _value;
 };
 
 
 inline Scalar operator + (const Scalar & v1, const Scalar & v2) {
     return Scalar(v1.value() + v2.value());
 }
 
 inline Scalar operator - (const Scalar & v1, const Scalar & v2) {
     return Scalar(v1.value() - v2.value());
 }
 
 inline Scalar operator * (const Scalar & v1, const Scalar & v2) {
     return Scalar(v1.value() * v2.value());
 }
 
 inline Scalar operator / (const Scalar & v1, const Scalar & v2) {
     return Scalar(v1.value() / v2.value());
 }
 
 
 }

 
 #endif
 