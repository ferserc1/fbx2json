# fbx2json

Generate a json to the standar output from a FBX file.

## Requirements

- Autodesk FBX SDK 2020.0: You can download it from Autodesk site.
- Windows: Visual Studio 2019.
- macOS: Xcode 8 or newer.
- Linux: CMake and build tools

The Visual Studio and Xcode projects are configured to work out of the box with Autodesk FBX SDK 2020.0, with the default installation settings. Maybe you will need to change some configuration of the project if you install another version of de Autodesk FBX SDK or if you change the default instalation path.

## FBX SDK Linux installation

Download FBX SDK from the Autodesk site and execute the installation script as follows:

```bash
$ cd [your fbx sdk download path]
$ mkdir -p [path to your fbx2json repo]/fbx/Linux/2020.0
$ ./fbx202001_fbxsdk_linux [path to your fbx2json repo]/fbx/Linux/2020.0
``` 

For example:

```bash
$ cd ~/Downloads
$ mkdir ~/develop/fbx2json/fbx/Linux/2020.0
$ ./fbx202001_fbxsdk_linux ~/develop/fbx2json/fbx/Linux/2020.0
``` 

Since fbxsdk linking is static, it is necessary to also add the libxml2 development package. For example, on Debian-based distributions:

```bash
sudo apt-get install libxml2-dev
```


## License (MIT)

Copyright 2019 Fernando Serrano Carpena

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## About Autodesk FBX SDK

This project is open source, but note that the Autodesk FBX SDK is not open source (but is free, and you can use it for commercial projects). To build fbx2json, you will need to download, install and accept the terms of the Autodesk FBX SDK license.

In addition, keep in mind that to distribute the final built version of this program with the fbx libraries to the end user, you must comply with the Autodkes FBX SDK license terms.

More info and download at: [https://www.autodesk.com/products/fbx/overview](https://www.autodesk.com/products/fbx/overview)