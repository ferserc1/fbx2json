
#include <iostream>
#include <fstream>

#include <fbxsdk.h>

#include <matrix.hpp>

#include <map>
#include <iomanip>

struct PolyList {
	std::vector<float> vertex;
	std::vector<float> normal;
	std::vector<float> uv0;
	std::vector<float> uv1;
	std::vector<float> uv2;
	std::vector<float> color;
	std::vector<unsigned int> indices;
	std::string name;

	void addVertex(const fbx2json::Vector3 & v) { vertex.push_back(v.x()); vertex.push_back(v.y()); vertex.push_back(v.z()); }
	void addNormal(const fbx2json::Vector3 & v) { normal.push_back(v.x()); normal.push_back(v.y()); normal.push_back(v.z()); }
	void addTexCoord0(const fbx2json::Vector2 & uv) { uv0.push_back(uv.x()); uv0.push_back(uv.y()); }
	void addTexCoord1(const fbx2json::Vector2 & uv) { uv1.push_back(uv.x()); uv1.push_back(uv.y()); }
	void addTexCoord2(const fbx2json::Vector2 & uv) { uv2.push_back(uv.x()); uv2.push_back(uv.y()); }
	void addColor(const fbx2json::Vector4 & c) { color.push_back(c.r()); color.push_back(c.g()); color.push_back(c.b()); color.push_back(c.a()); }
};

void printTabs(std::ostream&,int);
void printNode(FbxScene *, FbxNode * node, std::ostream & outStream, int tabs);
void printNodeAttributes(FbxScene *, FbxNode * node, std::ostream & outStream, int tabs);
void printAttribute(FbxScene *, FbxNodeAttribute * nodeAttrib, std::ostream & outStream, int tabs);
FbxString getAttributeTypeName(FbxNodeAttribute::EType type);
bool printMesh(FbxScene *, FbxNode * node, FbxMesh *, std::ostream &outStream, int tabs);
void printPolyList(PolyList * plist, std::ostream & outStream, int tabs);

int main(int argc, char ** argv) {
	if (argc == 1) {
		std::cerr << "Usage: fbx2json file.fbx" << std::endl;
		return 1;
	}
	std::string filePath = argv[1];
	std::ostream * outStream = &std::cout;
	std::fstream outFile;

	if (argc >= 3) {
		std::string outFilePath(argv[2]);
		outFile.open(outFilePath, std::ofstream::out);
		if (outFile.good()) {
			outStream = &outFile;
		}
		else {
			std::cerr << "Could not open output file" << std::endl;
			return -1;
		}
	}

	FbxManager * lSdkManager = FbxManager::Create();
	FbxIOSettings * ios = FbxIOSettings::Create(lSdkManager, IOSROOT);
	lSdkManager->SetIOSettings(ios);

	FbxImporter * lImporter = FbxImporter::Create(lSdkManager, "");

	if (!lImporter->Initialize(filePath.c_str(), -1, lSdkManager->GetIOSettings())) {
		std::cerr << "Cannot initialize fbx importer." << std::endl;
		std::cerr << "Error returned: " << lImporter->GetStatus().GetErrorString() << std::endl;
		return -1;
	}

	FbxScene * lScene = FbxScene::Create(lSdkManager, "fbx2jsonScene");

	lImporter->Import(lScene);

	lImporter->Destroy();

	FbxNode * lRootNode = lScene->GetRootNode();
	if (lRootNode) {
		(*outStream) << "[" << std::endl;
		for (auto i = 0; i < lRootNode->GetChildCount(); ++i) {
			printNode(lScene,lRootNode->GetChild(i), *outStream, 1);
			if (i < lRootNode->GetChildCount() - 1) {
				(*outStream) << ",";
			}
			(*outStream) << std::endl;
		}
		(*outStream) << "]" << std::endl;
	}

	lSdkManager->Destroy();

	if (outFile.is_open()) {
		outFile.close();
	}

	return 0;
}

template <class T>
void printVector(const std::vector<T> & v, std::ostream & outStream) {
	outStream << "[";

	for (auto i = 0; i < v.size(); ++i) {
		outStream << v[i];
		if (i<v.size() - 1) {
			outStream << ",";
		}
	}
	outStream << "]";
}

void printTabs(std::ostream & outStream, int tabs) {
	for (auto i = 0; i < tabs; ++i) outStream << "\t";
}

void printNode(FbxScene * scene, FbxNode * node, std::ostream & outStream, int tabs) {
	printTabs(outStream, tabs);
	outStream << "{" << std::endl;
	const char * nodeName = node->GetName();
	FbxDouble3 translation = node->LclTranslation.Get();
	FbxDouble3 rotation = node->LclRotation.Get();
	FbxDouble3 scaling = node->LclScaling.Get();
	
	printTabs(outStream, tabs+1);
	outStream << "\"name\":\"" << nodeName << "\"," << std::endl;
	printTabs(outStream, tabs+1);
	outStream << "\"translation\":[" << translation[0] << "," << translation[1] << "," << translation[2] << "]," << std::endl;
	printTabs(outStream, tabs+1);
	outStream << "\"rotation\":[" << rotation[0] << "," << rotation[1] << "," << rotation[2] << "]," << std::endl;
	printTabs(outStream, tabs+1);
	outStream << "\"scaling\":[" << scaling[0] << "," << scaling[1] << "," << scaling[2] << "]," << std::endl;

	printNodeAttributes(scene, node, outStream, tabs + 1);

	FbxMesh * mesh = node->GetMesh();
	if (mesh) {
		outStream << std::endl;
		if (!printMesh(scene, node, mesh, outStream, tabs + 1)) {
			std::cerr << "Mesh skipped due to invalid FBX format error" << std::endl;
		}
	}
	
	outStream << std::endl;
	printTabs(outStream, tabs + 1);
	outStream << "\"children\":[" << std::endl;
	for (int i = 0; i < node->GetChildCount(); i++) {
		printNode(scene, node->GetChild(i), outStream, tabs + 2);
		if (i < node->GetChildCount() - 1) {
			outStream << ",";
		}
		outStream << std::endl;
	}
	printTabs(outStream, tabs + 1);
	outStream << "]" << std::endl;

	outStream << std::endl;

	printTabs(outStream, tabs);
	outStream << "}";
}

void printNodeAttributes(FbxScene * scene, FbxNode * node, std::ostream & outStream, int tabs) {
	printTabs(outStream, tabs);
	outStream << "\"attributes\":[" << std::endl;

	for (int i = 0; i < node->GetNodeAttributeCount(); ++i) {
		FbxNodeAttribute * attr = node->GetNodeAttributeByIndex(i);
		printAttribute(scene, attr, outStream, tabs + 1);
	}

	printTabs(outStream, tabs);
	outStream << "],";
}

void printAttribute(FbxScene * scene, FbxNodeAttribute * attrib, std::ostream & outStream, int tabs) {
	if (!attrib) return;
	printTabs(outStream, tabs);
	outStream << "{" << std::endl;

	FbxString typeName = getAttributeTypeName(attrib->GetAttributeType());
	FbxString attrName = attrib->GetName();

	printTabs(outStream, tabs + 1);
	outStream << "\"type\":\"" << typeName.Buffer() << "\"," << std::endl;
	printTabs(outStream, tabs + 1);
	outStream << "\"name\":\"" << attrName.Buffer() << "\"" << std::endl;

	printTabs(outStream, tabs);
	outStream << "}" << std::endl;
}

bool printMesh(FbxScene * scene, FbxNode * node, FbxMesh * mesh, std::ostream &outStream, int tabs) {
	using namespace fbx2json;
	FbxAnimEvaluator * sceneEval = scene->GetAnimationEvaluator();
	FbxAMatrix & trx = sceneEval->GetNodeGlobalTransform(node);

	double * val = reinterpret_cast<double*>(trx.Buffer());


	Matrix4 matrix(static_cast<float>(val[0]), static_cast<float>(val[1]), static_cast<float>(val[2]), static_cast<float>(val[3]),
		static_cast<float>(val[4]), static_cast<float>(val[5]), static_cast<float>(val[6]), static_cast<float>(val[7]),
		static_cast<float>(val[8]), static_cast<float>(val[9]), static_cast<float>(val[10]), static_cast<float>(val[11]),
		static_cast<float>(val[12]), static_cast<float>(val[13]), static_cast<float>(val[14]), static_cast<float>(val[15]));

	printTabs(outStream, tabs);
	outStream << "\"transform\":[" << val[0] << "," << val[1] << "," << val[2] << "," << val[3] << ","
		<< val[4] << "," << val[5] << "," << val[6] << "," << val[7] << ","
		<< val[8] << "," << val[9] << "," << val[10] << "," << val[11] << ","
		<< val[12] << "," << val[13] << "," << val[14] << "," << val[15] << "]," << std::endl;

	int polygonCount = mesh->GetPolygonCount();
	int vertexCounter = 0;
	int ctrlPointCount = mesh->GetControlPointsCount();

	std::vector<Vector3> vertexList;
	for (int i = 0; i < ctrlPointCount; ++i) {
		float x = static_cast<float>(mesh->GetControlPointAt(i).mData[0]);
		float y = static_cast<float>(mesh->GetControlPointAt(i).mData[1]);
		float z = static_cast<float>(mesh->GetControlPointAt(i).mData[2]);

		vertexList.push_back(Vector3(x, y, z));
	}

	unsigned int index = 0;
	int texChannels = mesh->GetElementUVCount();
	FbxStringList lUVSetNameList;
	mesh->GetUVSetNames(lUVSetNameList);
	FbxLayerElementArrayTemplate<int> * indices;
	mesh->GetMaterialIndices(&indices);
	int matCount = 0;
	if (indices) {
		matCount = indices->GetCount();
	}
	else {
		std::cerr << "Warning: Invalid FBX format. No material indices found. If you used Autodesk 3D MAX to export your model, you must to apply materials to all the meshes." << std::endl;
		return false;
	}


	std::map<int, PolyList *> plistMap;

	PolyList * currentPlist = nullptr;
	for (int i = 0; i < polygonCount; ++i) {
		int size = mesh->GetPolygonSize(i);
		int firstPoint = 0;
		int firstPointVC;
		int prevCtrlPoint = 0;
		int prevPointVC;
		int matIndex = indices->GetAt(i);
		if (plistMap.find(matIndex) == plistMap.end()) {
			PolyList * newPlist = new PolyList();
			plistMap[matIndex] = newPlist;
			auto mat = node->GetMaterial(matIndex);
			newPlist->name = mat ? mat->GetName() : "";
		}
		currentPlist = plistMap[matIndex];
		index = static_cast<unsigned int>(currentPlist->indices.size());
		for (int j = 0; j < size; ++j) {
			Vector3 normal;
			Vector2 uv;
			FbxVector4 vn;
			FbxVector2 fbxUv;
			bool um;
			int ctrlPointIndex = mesh->GetPolygonVertex(i, j);
			int texUvIndex = mesh->GetTextureUVIndex(i, j);

			Vector3 vector = vertexList[ctrlPointIndex];
			if (j == 0) {
				firstPoint = static_cast<unsigned int>(ctrlPointIndex);
				firstPointVC = vertexCounter;
			}
			if (j < 3) {
				mesh->GetPolygonVertexNormal(i, j, vn);
				normal.set(static_cast<float>(vn.mData[0]), static_cast<float>(vn.mData[1]), static_cast<float>(vn.mData[2]));
				currentPlist->addVertex(vector);
				currentPlist->addNormal(normal);
				for (int iuv = 0; iuv < lUVSetNameList.GetCount(); ++iuv) {
					mesh->GetPolygonVertexUV(i, j, lUVSetNameList.GetStringAt(iuv), fbxUv, um);
					uv.set(static_cast<float>(fbxUv.mData[0]), static_cast<float>(fbxUv.mData[1]));
					if (iuv == 0) {
						currentPlist->addTexCoord0(uv);
					}
					else if (iuv == 1) {
						currentPlist->addTexCoord1(uv);
					}
					else if (iuv == 2) {
						currentPlist->addTexCoord2(uv);
					}
				}
				currentPlist->indices.push_back(index++);
			}
			else {
				mesh->GetPolygonVertexNormal(i, j - 1, vn);
				normal.set(static_cast<float>(vn.mData[0]), static_cast<float>(vn.mData[1]), static_cast<float>(vn.mData[2]));
				currentPlist->addVertex(vertexList[prevCtrlPoint]);
				currentPlist->addNormal(normal);
				for (int iuv = 0; iuv < lUVSetNameList.GetCount(); ++iuv) {
					mesh->GetPolygonVertexUV(i, j - 1, lUVSetNameList.GetStringAt(iuv), fbxUv, um);
					uv.set(static_cast<float>(fbxUv.mData[0]), static_cast<float>(fbxUv.mData[1]));
					if (iuv == 0) {
						currentPlist->addTexCoord0(uv);
					}
					else if (iuv == 1) {
						currentPlist->addTexCoord1(uv);
					}
					else if (iuv == 2) {
						currentPlist->addTexCoord2(uv);
					}
				}
				currentPlist->indices.push_back(index++);

				mesh->GetPolygonVertexNormal(i, j, vn);
				normal.set(static_cast<float>(vn.mData[0]), static_cast<float>(vn.mData[1]), static_cast<float>(vn.mData[2]));
				currentPlist->addVertex(vertexList[ctrlPointIndex]);
				currentPlist->addNormal(normal);
				for (int iuv = 0; iuv < lUVSetNameList.GetCount(); ++iuv) {
					mesh->GetPolygonVertexUV(i, j, lUVSetNameList.GetStringAt(iuv), fbxUv, um);
					uv.set(static_cast<float>(fbxUv.mData[0]), static_cast<float>(fbxUv.mData[1]));
					if (iuv == 0) {
						currentPlist->addTexCoord0(uv);
					}
					else if (iuv == 1) {
						currentPlist->addTexCoord1(uv);
					}
				}
				currentPlist->indices.push_back(index++);

				mesh->GetPolygonVertexNormal(i, 0, vn);
				normal.set(static_cast<float>(vn.mData[0]), static_cast<float>(vn.mData[1]), static_cast<float>(vn.mData[2]));
				currentPlist->addVertex(vertexList[firstPoint]);
				currentPlist->addNormal(normal);
				for (int iuv = 0; iuv < lUVSetNameList.GetCount(); ++iuv) {
					mesh->GetPolygonVertexUV(i, 0, lUVSetNameList.GetStringAt(iuv), fbxUv, um);
					uv.set(static_cast<float>(fbxUv.mData[0]), static_cast<float>(fbxUv.mData[1]));
					if (iuv == 0) {
						currentPlist->addTexCoord0(uv);
					}
					else if (iuv == 1) {
						currentPlist->addTexCoord1(uv);
					}
				}
				currentPlist->indices.push_back(index++);
			}
			prevCtrlPoint = static_cast<unsigned int>(ctrlPointIndex);
			prevPointVC = vertexCounter;
			++vertexCounter;
		}
	}

	printTabs(outStream, tabs);
	outStream << "\"meshData\":[" << std::endl;
	std::string sep = "";
	for (auto pl : plistMap) {
		PolyList * plist = pl.second;
		outStream << sep << std::endl;
		printPolyList(plist, outStream, tabs + 1);
		sep = ",";
	}
	printTabs(outStream, tabs);
	outStream << "]," << std::endl;

	return true;
}

void printPolyList(PolyList * plist, std::ostream & outStream, int tabs) {
	printTabs(outStream,tabs);
	outStream << "{" << std::endl;

	printTabs(outStream, tabs + 1);
	outStream << "\"name\":\"" << plist->name << "\"";

	if (plist->vertex.size() > 0) {
		outStream << "," << std::endl;
		printTabs(outStream, tabs + 1);
		outStream << "\"vertex\":";
		printVector<float>(plist->vertex, outStream);
	}
	if (plist->normal.size() > 0) {
		outStream << "," << std::endl;
		printTabs(outStream, tabs + 1);
		outStream << "\"normal\":";
		printVector<float>(plist->normal, outStream);
	}
	if (plist->uv0.size() > 0) {
		outStream << "," << std::endl;
		printTabs(outStream, tabs + 1);
		outStream << "\"texCoord0\":";
		printVector<float>(plist->uv0, outStream);
	}
	if (plist->uv1.size() > 0) {
		outStream << "," << std::endl;
		printTabs(outStream, tabs + 1);
		outStream << "\"texCoord1\":";
		printVector<float>(plist->uv1, outStream);
	}
	if (plist->uv2.size() > 0) {
		outStream << "," << std::endl;
		printTabs(outStream, tabs + 1);
		outStream << "\"texCoord2\":";
		printVector<float>(plist->uv2, outStream);
	}
	if (plist->indices.size() > 0) {
		outStream << "," << std::endl;
		printTabs(outStream, tabs + 1);
		outStream << "\"indices\":";
		printVector<unsigned int>(plist->indices, outStream);
		outStream << std::endl;
	}

	printTabs(outStream, tabs);
	outStream << "}";
}

FbxString getAttributeTypeName(FbxNodeAttribute::EType type) {
	switch (type) {
	case FbxNodeAttribute::eUnknown: return "unidentified";
	case FbxNodeAttribute::eNull: return "null";
	case FbxNodeAttribute::eMarker: return "marker";
	case FbxNodeAttribute::eSkeleton: return "skeleton";
	case FbxNodeAttribute::eMesh: return "mesh";
	case FbxNodeAttribute::eNurbs: return "nurbs";
	case FbxNodeAttribute::ePatch: return "patch";
	case FbxNodeAttribute::eCamera: return "camera";
	case FbxNodeAttribute::eCameraStereo: return "stereo";
	case FbxNodeAttribute::eCameraSwitcher: return "camera switcher";
	case FbxNodeAttribute::eLight: return "light";
	case FbxNodeAttribute::eOpticalReference: return "optical reference";
	case FbxNodeAttribute::eOpticalMarker: return "marker";
	case FbxNodeAttribute::eNurbsCurve: return "nurbs curve";
	case FbxNodeAttribute::eTrimNurbsSurface: return "trim nurbs surface";
	case FbxNodeAttribute::eBoundary: return "boundary";
	case FbxNodeAttribute::eNurbsSurface: return "nurbs surface";
	case FbxNodeAttribute::eShape: return "shape";
	case FbxNodeAttribute::eLODGroup: return "lodgroup";
	case FbxNodeAttribute::eSubDiv: return "subdiv";
	default: return "unknown";
	}
}
